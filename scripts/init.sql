DROP DATABASE IF EXISTS drive_school;

CREATE DATABASE IF NOT EXISTS drive_school CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE drive_school;

CREATE TABLE IF NOT EXISTS Categoria
(
    id     INT         NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS Instructor
(
    id                 INT         NOT NULL AUTO_INCREMENT,
    nombre             VARCHAR(50) NOT NULL,
    apellidos          VARCHAR(50) NOT NULL,
    sexo               CHAR(1)     NOT NULL,
    telefono           VARCHAR(10) NOT NULL,
    edad               INT         NOT NULL,
    codigo_control     VARCHAR(10) NOT NULL,
    tipo               VARCHAR(50) NOT NULL,
    correo_electronico VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS Materia
(
    id     INT         NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    tipo   VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS Horario
(
    id          INT         NOT NULL AUTO_INCREMENT,
    dias        VARCHAR(50) NOT NULL,
    hora_inicio TIME        NOT NULL,
    hora_fin    TIME        NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS Cliente
(
    id        INT         NOT NULL AUTO_INCREMENT,
    nombre    VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50) NOT NULL,
    sexo      CHAR(1)     NOT NULL,
    telefono  VARCHAR(10) NOT NULL,
    edad      INT         NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS Coche
(
    id                  INT         NOT NULL AUTO_INCREMENT,
    matricula           VARCHAR(10) NOT NULL,
    modelo              VARCHAR(50) NOT NULL,
    fecha_mantenimiento DATE        NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS Grupo
(
    id            INT         NOT NULL AUTO_INCREMENT,
    nombre        VARCHAR(50) NOT NULL,
    id_horario    INT         NOT NULL,
    id_materia    INT         NOT NULL,
    id_instructor INT         NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id_materia) REFERENCES Materia (id),
    FOREIGN KEY (id_instructor) REFERENCES Instructor (id),
    FOREIGN KEY (id_horario) REFERENCES Horario (id)
);

CREATE TABLE IF NOT EXISTS Asignacion
(
    id       INT NOT NULL AUTO_INCREMENT,
    id_grupo INT NOT NULL,
    id_coche INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id_grupo) REFERENCES Grupo (id),
    FOREIGN KEY (id_coche) REFERENCES Coche (id)
);

CREATE TABLE IF NOT EXISTS Inscripcion
(
    id           INT NOT NULL AUTO_INCREMENT,
    id_cliente   INT NOT NULL,
    id_grupo     INT NOT NULL,
    id_categoria INT NOT NULL,
    nota         INT NOT NULL DEFAULT 0,
    PRIMARY KEY (id),
    FOREIGN KEY (id_cliente) REFERENCES Cliente (id),
    FOREIGN KEY (id_grupo) REFERENCES Grupo (id),
    FOREIGN KEY (id_categoria) REFERENCES Categoria (id)
);

CREATE TABLE IF NOT EXISTS Historial_Notas
(
    id                   INT         NOT NULL AUTO_INCREMENT,
    nombre_grupo         VARCHAR(50) NOT NULL,
    nombre_instructor    VARCHAR(50) NOT NULL,
    apellidos_instructor VARCHAR(50) NOT NULL,
    nombre_estudiante    VARCHAR(50) NOT NULL,
    apellidos_estudiante VARCHAR(50) NOT NULL,
    materia              VARCHAR(50) NOT NULL,
    tipo_materia         VARCHAR(50) NOT NULL,
    nota                 INT         NOT NULL,
    fecha                DATE        NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS Nombre_Compania
(
    id           INT         NOT NULL AUTO_INCREMENT,
    nombre       VARCHAR(50) NOT NULL,
    fecha_cambio DATETIME    NOT NULL,
    PRIMARY KEY (id)
);


-- Realizar una función que me devuelva el numero de clientes inscrito en una materia dada.

DELIMITER //
CREATE
    DEFINER = `root`@`localhost` FUNCTION `fn_numero_clientes_inscritos`(id_materia INT) RETURNS int
    READS SQL DATA
    DETERMINISTIC
BEGIN
    DECLARE num_clientes INT;

    SELECT COUNT(*)
    INTO num_clientes
    FROM Inscripcion
             JOIN Grupo ON Inscripcion.id_grupo = Grupo.id
    WHERE Grupo.id_materia = id_materia;

    RETURN num_clientes;
END;
//
DELIMITER ;

-- Realizar una función que me devuelva el número de clientes reprobadas o aprobadas dado por un instructor en específico.

DELIMITER //
CREATE
    DEFINER = `root`@`localhost` FUNCTION `fn_numero_clientes_aprobados_reprobados`(id_instructor INT, aprobado TINYINT) RETURNS int
    READS SQL DATA
    DETERMINISTIC
BEGIN
    DECLARE num_clientes INT;

    IF aprobado = 1 THEN
        SELECT COUNT(*)
        INTO num_clientes
        FROM Inscripcion I
                 JOIN Grupo G ON I.id_grupo = G.id
        WHERE G.id_instructor = id_instructor
          AND I.nota >= 70;
    ELSE
        SELECT COUNT(*)
        INTO num_clientes
        FROM Inscripcion I
                 JOIN Grupo G ON I.id_grupo = G.id
        WHERE G.id_instructor = id_instructor
          AND I.nota < 70;
    END IF;

    RETURN num_clientes;
END;
//
DELIMITER ;

-- Realizar un trigger para que guarde el historial de las notas de los clientes cursados en un grupo tanto teórico o practico dictado por su instructor correspondiente.

DELIMITER //
CREATE TRIGGER tr_guardar_historial_notas
    AFTER UPDATE
    ON Inscripcion
    FOR EACH ROW
BEGIN
    IF (OLD.nota <> NEW.nota) THEN
        INSERT INTO Historial_Notas
        (nombre_grupo, nombre_instructor, apellidos_instructor, nombre_estudiante, apellidos_estudiante, materia,
         tipo_materia, nota, fecha)
        SELECT G.nombre,
               Ins.nombre,
               Ins.apellidos,
               C.nombre,
               C.apellidos,
               M.nombre,
               M.tipo,
               NEW.nota,
               CURDATE()
        FROM Inscripcion I
                 JOIN Grupo G ON I.id_grupo = G.id
                 JOIN Cliente C ON I.id_cliente = C.id
                 JOIN Materia M ON G.id_materia = M.id
                 JOIN Instructor Ins ON G.id_instructor = Ins.id
        WHERE I.id = NEW.id;
    END IF;
END;
//
DELIMITER ;

-- Realizar un trigger que permita inscribir a clientes solo mayores a 18 años.

DELIMITER //
CREATE TRIGGER tr_validar_edad
    BEFORE INSERT
    ON Cliente
    FOR EACH ROW
BEGIN
    IF (NEW.edad < 18) THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'El cliente debe ser mayor de 18 años.';
    END IF;
END;
//
DELIMITER ;
