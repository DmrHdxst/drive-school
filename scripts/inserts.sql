use drive_school;

-- Inserciones en la tabla Categoría
INSERT INTO Categoria (nombre)
VALUES ('A'),
       ('P'),
       ('M');

-- Inserciones en la tabla Materia
INSERT INTO Materia (nombre, tipo)
VALUES ('Normas de Tránsito', 'Teoría'),
       ('Teoría Automotriz', 'Teoría'),
       ('Práctica de Conducción', 'Práctica');

-- Inserciones en la tabla Horario (dos días por fila)
INSERT INTO Horario (dias, hora_inicio, hora_fin)
VALUES ('Lunes, Miércoles', '08:00:00', '10:00:00'),
       ('Martes, Jueves', '10:30:00', '12:30:00'),
       ('Lunes, Miércoles', '14:00:00', '16:00:00'),
       ('Martes, Jueves', '16:30:00', '18:30:00');

-- Inserciones en la tabla Instructor (restricciones de tipo de instructor)
INSERT INTO Instructor (nombre, apellidos, sexo, telefono, edad, codigo_control, tipo, correo_electronico)
VALUES ('Juan', 'Lopez', 'M', '9876543210', 35, 'XYZ789', 'Teoría', 'juan.lopez@gmail.com'),
       ('Maria', 'Gomez', 'F', '5551112233', 26, 'DEF456', 'Práctica', 'maria.gomez@gmail.com'),
       ('Ariel', 'Blanco', 'M', '7778889999', 29, 'GHI789', 'Práctica', 'ariel.blanco@gmail.com'),
       ('Ana', 'Rodriguez', 'F', '6667778888', 28, 'JKL012', 'Teoría', 'ana.rodriguez@gmail.com');

-- Inserciones en la tabla Cliente
INSERT INTO Cliente (nombre, apellidos, sexo, telefono, edad)
VALUES ('Juan', 'Gonzalez', 'M', '3334445555', 22),
       ('Pedro', 'Martinez', 'M', '9998887777', 30),
       ('Luisa', 'Perez', 'F', '1112223333', 25);

-- Inserciones en la tabla Coche (información detallada de coches)
INSERT INTO Coche (matricula, modelo, fecha_mantenimiento)
VALUES ('ABC123', 'Toyota Corolla', '2023-02-15'),
       ('XYZ789', 'Honda Civic', '2023-03-20'),
       ('DEF456', 'Ford Focus', '2023-01-10'),
       ('GHI789', 'Chevrolet Malibu', '2023-04-05');

-- Inserciones en la tabla Grupo
INSERT INTO Grupo (nombre, id_horario, id_materia, id_instructor)
VALUES ('Grupo A', 1, 1, 1),
       ('Grupo B', 2, 2, 4),
       ('Grupo C', 3, 3, 2),
       ('Grupo D', 4, 3, 3);

-- Inserciones en la tabla Asignacion (coches solo para materias prácticas)
INSERT INTO Asignacion (id_grupo, id_coche)
SELECT G.id, C.id
FROM Grupo G
         JOIN Materia M ON G.id_materia = M.id
         JOIN Coche C ON M.tipo = 'Práctica';

-- Inserciones en la tabla Inscripcion (asignando clientes a grupos correspondientes)
INSERT INTO Inscripcion (id_cliente, id_grupo, id_categoria, nota)
VALUES (1, 1, 1, 90), -- Juan Gonzalez inscrito en Grupo A (Teoría)
       (1, 3, 3, 78), -- Juan Gonzalez inscrito en Grupo C (Práctica)
       (2, 2, 2, 65), -- Pedro Martinez inscrito en Grupo B (Teoría)
       (2, 3, 3, 62), -- Pedro Martinez inscrito en Grupo C (Práctica)
       (3, 1, 1, 75), -- Luisa Perez inscrita en Grupo A (Teoría)
       (3, 4, 3, 68);
-- Luisa Perez inscrita en Grupo D (Práctica)

-- Inserciones en la tabla HistorialNotas (usando DATE con formato de fecha)
INSERT INTO Historial_Notas (nombre_grupo, nombre_instructor, apellidos_instructor, nombre_estudiante,
                             apellidos_estudiante, materia, tipo_materia, nota, fecha)
SELECT G.nombre,
       Ins.nombre,
       Ins.apellidos,
       C.nombre,
       C.apellidos,
       M.nombre,
       M.tipo,
       I.nota,
       CURDATE() AS fecha
FROM Inscripcion I
         JOIN Grupo G ON I.id_grupo = G.id
         JOIN Cliente C ON I.id_cliente = C.id
         JOIN Materia M ON G.id_materia = M.id
         JOIN Instructor Ins ON G.id_instructor = Ins.id;

-- Inserciones en la tabla Nombre_compania
INSERT INTO Nombre_Compania (nombre, fecha_cambio)
VALUES ('Drive School Corp', NOW());
