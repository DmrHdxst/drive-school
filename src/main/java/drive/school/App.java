package drive.school;

import drive.school.ui.TerminalUi;
import java.io.IOException;
import java.sql.SQLException;

public class App {

  public static void main(String[] args) throws SQLException, IOException {
    TerminalUi app = new TerminalUi();
    app.run();
  }
}
