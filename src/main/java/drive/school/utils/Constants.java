package drive.school.utils;

public class Constants {

  public static final String URL = "jdbc:mysql://localhost:9090/drive_school";

  public static final String USERNAME = "root";

  public static final String PASSWORD = "root";

  public static final String MENU = """
          ---------- Drive School ----------
          1. Create a Instructor
          2. Create a Client
          3. List all Clients by Instructor
          4. Update Instructor's email
          5. Delete Instructors without Clients
          6. List all Clients
          7. List all Instructors
          8. Exit
          Please select an option (1-8):
          """;
}
