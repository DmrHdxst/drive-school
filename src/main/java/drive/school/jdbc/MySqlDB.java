package drive.school.jdbc;

import static drive.school.utils.Constants.PASSWORD;
import static drive.school.utils.Constants.URL;
import static drive.school.utils.Constants.USERNAME;

import drive.school.records.Client;
import drive.school.records.Instructor;
import drive.school.tables.ClientsTable;
import drive.school.tables.InstructorsTable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySqlDB {

  private final Connection connection;

  public MySqlDB() throws SQLException {
    this.connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
  }

  public void insertInstructor(Instructor instructor) {
    String insertInstructorQuery = "INSERT INTO Instructor (nombre, apellidos, sexo, telefono, edad, codigo_control, tipo, correo_electronico) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    try (PreparedStatement preparedStatement = connection.prepareStatement(insertInstructorQuery)) {
      preparedStatement.setString(1, instructor.name());
      preparedStatement.setString(2, instructor.lastName());
      preparedStatement.setString(3, instructor.sex());
      preparedStatement.setString(4, instructor.phone());
      preparedStatement.setInt(5, instructor.age());
      preparedStatement.setString(6, instructor.controlCode());
      preparedStatement.setString(7, instructor.type());
      preparedStatement.setString(8, instructor.email());
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new RuntimeException("Error inserting instructor " + e.getMessage());
    }
  }

  public void insertClient(Client client) {
    String insertClientQuery = "INSERT INTO Cliente (nombre, apellidos, sexo, telefono, edad) VALUES (?, ?, ?, ?, ?)";
    try (PreparedStatement preparedStatement = connection.prepareStatement(insertClientQuery)) {
      preparedStatement.setString(1, client.name());
      preparedStatement.setString(2, client.lastName());
      preparedStatement.setString(3, client.sex());
      preparedStatement.setString(4, client.phone());
      preparedStatement.setInt(5, client.age());
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new RuntimeException("Error inserting client " + e.getMessage());
    }
  }

  public ClientsTable listAllClientsByInstructor(String instructorName, String instructorLastName) {
    String listAllClientsByInstructorQuery = "SELECT C.nombre AS client_name, C.apellidos AS client_lastname, C.edad AS client_age, C.sexo AS client_sex, C.telefono AS client_phone " + "FROM Instructor Ins " + "JOIN Grupo G ON Ins.id = G.id_instructor " + "JOIN Inscripcion I ON I.id_grupo = G.id " + "JOIN Cliente C ON I.id_cliente = C.id " + "WHERE Ins.nombre = ? AND Ins.apellidos = ?";
    ClientsTable clients = new ClientsTable();
    try (PreparedStatement preparedStatement = connection.prepareStatement(listAllClientsByInstructorQuery)) {
      preparedStatement.setString(1, instructorName);
      preparedStatement.setString(2, instructorLastName);
      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        String clientName = rs.getString("client_name");
        String clientLastname = rs.getString("client_lastname");
        String clientSex = rs.getString("client_sex");
        String clientPhone = rs.getString("client_phone");
        int clientAge = rs.getInt("client_age");
        clients.add(new Client(clientName, clientLastname, clientSex, clientPhone, clientAge));
      }
    } catch (SQLException e) {
      throw new RuntimeException("Error listing all clients by instructor " + e.getMessage());
    }
    return clients;
  }

  public void updateInstructorEmail(String nombreInstructor, String apellidosInstructor, String nuevoCorreo) {
    String updateEmailQuery = "UPDATE Instructor SET correo_electronico = ? WHERE nombre = ? AND apellidos = ?";
    try (PreparedStatement preparedStatement = connection.prepareStatement(updateEmailQuery)) {
      preparedStatement.setString(1, nuevoCorreo);
      preparedStatement.setString(2, nombreInstructor);
      preparedStatement.setString(3, apellidosInstructor);
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new RuntimeException("Error updating email " + e.getMessage());
    }
  }

  public void deleteInstructorsWithoutClients() {
    String deleteInstructorsWithoutClientsQuery = "DELETE FROM Instructor WHERE id NOT IN (SELECT DISTINCT id FROM Inscripcion)";
    try (PreparedStatement preparedStatement = connection.prepareStatement(deleteInstructorsWithoutClientsQuery)) {
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new RuntimeException("Error deleting instructors without clients " + e.getMessage());
    }
  }

  public ClientsTable getAllClients() {
    String getAllClientsQuery = "SELECT * FROM Cliente";
    try (PreparedStatement preparedStatement = connection.prepareStatement(getAllClientsQuery)) {
      ClientsTable clients = new ClientsTable();
      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        String clientName = rs.getString("nombre");
        String clientLastname = rs.getString("apellidos");
        String clientSex = rs.getString("sexo");
        String clientPhone = rs.getString("telefono");
        int clientAge = rs.getInt("edad");
        clients.add(new Client(clientName, clientLastname, clientSex, clientPhone, clientAge));
      }
      return clients;
    } catch (SQLException e) {
      throw new RuntimeException("Error getting all clients " + e.getMessage());
    }
  }

  public InstructorsTable getAllInstructors() {
    String getAllInstructorsQuery = "SELECT * FROM Instructor";
    try (PreparedStatement preparedStatement = connection.prepareStatement(getAllInstructorsQuery)) {
      InstructorsTable instructors = new InstructorsTable();
      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        String instructorName = rs.getString("nombre");
        String instructorLastname = rs.getString("apellidos");
        String instructorSex = rs.getString("sexo");
        String instructorPhone = rs.getString("telefono");
        int instructorAge = rs.getInt("edad");
        String instructorControlCode = rs.getString("codigo_control");
        String instructorType = rs.getString("tipo");
        String instructorEmail = rs.getString("correo_electronico");
        instructors.add(new Instructor(instructorName, instructorLastname, instructorSex, instructorPhone, instructorAge, instructorControlCode, instructorType, instructorEmail));
      }
      return instructors;
    } catch (SQLException e) {
      throw new RuntimeException("Error getting all instructors " + e.getMessage());
    }
  }
}
