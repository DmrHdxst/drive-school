package drive.school.tables;

import drive.school.records.Client;
import java.util.ArrayList;

public class ClientsTable extends ArrayList<Client> {

  public ClientsTable() {
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("\n");
    sb.append("|%13s |%13s |%7s |%15s |%5s |".formatted("Name", "LastName", "Gender", "Phone", "Age"));
    for (Client client : this) {
      sb.append("\n");
      sb.append(client.toString());
    }
    sb.append("\n");
    return sb.toString();
  }
}
