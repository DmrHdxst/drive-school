package drive.school.tables;

import drive.school.records.Instructor;
import java.util.ArrayList;

public class InstructorsTable extends ArrayList<Instructor> {

  public InstructorsTable() {
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("\n");
    sb.append("|%13s |%13s |%7s |%15s |%5s |%15s |%15s |%30s |".formatted("Name", "LastName", "Gender", "Phone", "Age", "Control Code", "Type", "Email"));
    for (Instructor instructor : this) {
      sb.append("\n");
      sb.append(instructor.toString());
    }
    sb.append("\n");
    return sb.toString();
  }
}
