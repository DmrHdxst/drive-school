package drive.school.ui;

import static drive.school.utils.Constants.MENU;

import drive.school.jdbc.MySqlDB;
import drive.school.records.Client;
import drive.school.records.Instructor;
import drive.school.tables.ClientsTable;
import drive.school.tables.InstructorsTable;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.SQLException;

public class TerminalUi {

  private final BufferedReader reader;

  private final BufferedWriter writer;

  private final MySqlDB mySqlDB;

  public TerminalUi() throws SQLException {
    reader = new BufferedReader(new InputStreamReader(System.in));
    writer = new BufferedWriter(new OutputStreamWriter(System.out));
    this.mySqlDB = new MySqlDB();
  }

  public void run() throws IOException {
    while (true) {
      writer.write(MENU);
      writer.flush();
      String input = reader.readLine();
      switch (input) {
        case "1":
          writer.write("You selected the Option: Create a Instructor\n");
          writer.write("Please enter the following information:\n");
          writer.write("Name:\n");
          writer.flush();
          String name = reader.readLine();
          writer.write("Last Name:\n");
          writer.flush();
          String lastName = reader.readLine();
          writer.write("Sex:\n");
          writer.flush();
          String sex = reader.readLine();
          writer.write("Phone:\n");
          writer.flush();
          String phone = reader.readLine();
          writer.write("Age:\n");
          writer.flush();
          int age = Integer.parseInt(reader.readLine());
          writer.write("Control Code:\n");
          writer.flush();
          String controlCode = reader.readLine();
          writer.write("Type:\n");
          writer.flush();
          String type = reader.readLine();
          writer.write("Email:\n");
          writer.flush();
          String email = reader.readLine();
          mySqlDB.insertInstructor(new Instructor(name, lastName, sex, phone, age, controlCode, type, email));
          writer.write("Instructor created successfully!\n");
          writer.flush();
          break;
        case "2":
          writer.write("You selected the Option: Create a Client\n");
          writer.write("Please enter the following information\n");
          writer.write("Name:\n");
          writer.flush();
          String name1 = reader.readLine();
          writer.write("Last Name:\n");
          writer.flush();
          String lastName1 = reader.readLine();
          writer.write("Sex:\n");
          writer.flush();
          String sex1 = reader.readLine();
          writer.write("Phone:\n");
          writer.flush();
          String phone1 = reader.readLine();
          writer.write("Age:\n");
          writer.flush();
          int age1 = Integer.parseInt(reader.readLine());
          mySqlDB.insertClient(new Client(name1, lastName1, sex1, phone1, age1));
          writer.write("Client created successfully!\n");
          writer.flush();
          break;
        case "3":
          writer.write("You selected the Option: List all Clients by Instructor\n");
          writer.write("Please enter the following information\n");
          writer.write("Instructor Name:\n");
          writer.flush();
          String instructorName1 = reader.readLine();
          writer.write("Instructor Last Name:\n");
          writer.flush();
          String instructorLastName1 = reader.readLine();
          ClientsTable clients = mySqlDB.listAllClientsByInstructor(instructorName1, instructorLastName1);
          writer.write(clients + "\n");
          writer.write("Clients listed successfully!\n");
          writer.flush();
          break;
        case "4":
          writer.write("You selected the Option: Update Instructor's email\n");
          writer.write("Please enter the following information\n");
          writer.write("Instructor Name:\n");
          writer.flush();
          String instructorName = reader.readLine();
          writer.write("Instructor Last Name:\n");
          writer.flush();
          String instructorLastName = reader.readLine();
          writer.write("New Email:\n");
          writer.flush();
          String newEmail = reader.readLine();
          mySqlDB.updateInstructorEmail(instructorName, instructorLastName, newEmail);
          writer.write("Instructor's email updated successfully!\n");
          writer.flush();
          break;
        case "5":
          writer.write("You selected the Option: Delete Instructors without Clients\n");
          writer.flush();
          mySqlDB.deleteInstructorsWithoutClients();
          writer.write("Instructors without clients deleted successfully!\n");
          writer.flush();
          break;
        case "6":
          writer.write("You selected the Option: List all Clients\n");
          writer.flush();
          ClientsTable clients1 = mySqlDB.getAllClients();
          writer.write(clients1 + "\n");
          writer.write("Clients listed successfully!\n");
          writer.flush();
          break;
        case "7":
          writer.write("You selected the Option: List all Instructors\n");
          writer.flush();
          InstructorsTable instructors = mySqlDB.getAllInstructors();
          writer.write(instructors + "\n");
          writer.write("Instructors listed successfully!\n");
          writer.flush();
          break;
        case "8":
          writer.write("You selected the Option: Exit\n");
          writer.write("Exiting the program. Goodbye!\n");
          writer.flush();
          reader.close();
          writer.close();
          System.exit(0);
        default:
          writer.write("Invalid choice. Please select a valid option (1-6).\n");
          writer.flush();
      }
    }
  }
}
