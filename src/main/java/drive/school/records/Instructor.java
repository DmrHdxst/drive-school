package drive.school.records;

public record Instructor(String name, String lastName, String sex, String phone, int age,
                         String controlCode, String type, String email) {

  @Override
  public String toString() {
    return "|%13s |%13s |%7s |%15s |%5s |%15s |%15s |%30s |".formatted(name, lastName, sex, phone, age, controlCode, type, email);
  }
}
