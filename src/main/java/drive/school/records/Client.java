package drive.school.records;

public record Client(String name, String lastName, String sex, String phone, int age) {

  @Override
  public String toString() {
    return "|%13s |%13s |%7s |%15s |%5s |".formatted(name, lastName, sex, phone, age);
  }
}
